/***
 **Module Name:  AppDelegate
 **File Name :  AppDelegate.swift
 **Project :   Serta
 **Copyright(c) : Serta.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : AppDelegate, Project starts from here.
 */

import UIKit
import KeychainAccess

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var isFirstTime:Bool?
    var deviceId,uuid,userId,status:String!
    var bundleID = "deviceId"
    var venderId: String!
    var uuidofDevice:String!
    var udid = UIDevice.current.identifierForVendor?.uuidString
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        gotoCode()
        return true
    }
    
    func gotoCode()
    {
        if isConnectedToNetwork() == false
        {
            let alertView = CustomAlertView(frame: CGRect(x: 0, y: 0, width: 1920, height: 1080))
            alertView.setTitle(message: "No Network Detected Please check the Internet connection")
            self.window?.makeKeyAndVisible()
            self.window?.addSubview(alertView)
        }
        else
        {
            let activityView = ActivityView.init(frame:(self.window?.frame)!)
            self.window?.addSubview(activityView)
            var parameters = [String:[String:AnyObject]]()
            
            venderId = getVenderId()
            
            if venderId != ""
            {
                // uuidofDevice = venderId
            }
            else
            {
                setVenderId()
            }
            parameters = ["getActivationCode":["model":"AppleTV4Gen" as AnyObject,"manufacturer":"Apple" as AnyObject,"device_name":"AppleTV" as AnyObject, "device_id":venderId! as AnyObject,"device_mac_address":"mac" as AnyObject,"brand_name":"Apple" as AnyObject,"host_name":"app" as AnyObject,"display_name":"apple" as AnyObject, "serial_number":"1234" as AnyObject]]
       
            ApiManager.sharedManager.postDataWithJson(url: kActivationCodeUrl, parameters: parameters) { (responseDict, error,isDone)in
                if error == nil
                {
                    let dict = responseDict as! [String:String]
                    self.uuid = dict["uuid"]
                    self.deviceId = dict["device_id"]
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    if !((dict["pair_devce"]! as String).contains("active"))
                    {
                        let rootView = storyBoard.instantiateViewController(withIdentifier: "Code") as! CodeViewController
                        rootView.uuid = self.uuid
                        rootView.deviceId = self.deviceId
                        rootView.code = dict["code"]
                        let navigationController = UINavigationController(rootViewController: rootView)
                        self.window?.rootViewController = navigationController
                        self.window?.makeKeyAndVisible()
                    }
                    else
                    {
                        let uservalue = self.getUserId()
                        if (uservalue != "")
                        {
                            self.userId = uservalue
                            self.gotoMenu(userid: self.userId,deviceid: self.deviceId)
                        }
                    }
                }
                else
                {
                    let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                        UIAlertAction in
                        exit(0)
                    })
                    alertview.addAction(defaultAction)
                    self.window?.rootViewController?.present(alertview, animated: true, completion: nil)
                }
                
                activityView.removeFromSuperview()
            }
        }
    }
    
    //setVenderId
    func setVenderId() {
        let keychain = Keychain(service: bundleID)
        do {
            try keychain.set(udid!, key: bundleID)
            print("venderId set : key \(bundleID) and value: \(udid)")
            venderId = getVenderId()
        }
        catch let error {
            print("Could not save data in Keychain : \(error)")
        }
        print(venderId)
    }
    
    
    // getVenderId
    func getVenderId() -> String {
        
        let keychain = Keychain(service: bundleID)
        
        if try! keychain.get(bundleID) != nil
        {
            let token : String = try! keychain.get(bundleID)!
            
            return token
            
        }
        return ""
        
    }
    //getUserid
    func getUserId() -> String {
        
        let keychain = Keychain(service: "userId")
        
        if try! keychain.get("userId") != nil
        {
            let token : String = try! keychain.get("userId")!
            
            return token
        }
        return ""
        
    }
    
    
    // go to Landing controller
    func gotoMenu(userid:String,deviceid:String)
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let rootView = storyBoard.instantiateViewController(withIdentifier: "Main") as! MainViewController
        rootView.deviceId = deviceid
        rootView.userId = userid
        rootView.uuid = self.uuid
        let navigationController = UINavigationController(rootViewController: rootView)
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

