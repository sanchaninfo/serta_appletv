/***
 **Module Name: orderSummaryViewController.
 **File Name :  orderSummaryViewController.swift
 **Project :   Serta
 **Copyright(c) : Serta.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : Order summary page.
 */
import UIKit
import Kingfisher

class StoreorderSummaryViewController: UIViewController {

    @IBOutlet weak var orderIDView: UIView!
    @IBOutlet weak var orderView: UIView!
    @IBOutlet weak var QuantityLbl: UILabel!
    @IBOutlet weak var sizeLbl: UILabel!
    @IBOutlet weak var clrLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var prodTtl: UILabel!
    @IBOutlet weak var placedDteLbl: UILabel!
    @IBOutlet weak var orderLbl: UILabel!
    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var ImageView: UIView!
    @IBOutlet weak var continueShopBtn: UIButton!
    @IBOutlet weak var shippingLbl: UILabel!
    @IBOutlet weak var shipLbl: UILabel!
     @IBOutlet weak var outView: UIView!
 
    var billingAddress = NSDictionary()
    var shippingAddress = NSDictionary()
    var color = UIColor()
    var productID = String()
    var orderDetails = NSDictionary()
    var fromPlayLayer = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        outView.layer.borderWidth = 2.0
        outView.layer.borderColor = UIColor.init(red: 228/255, green: 228/255, blue: 228/255, alpha: 1.0).cgColor

    //  orderView.backgroundColor = UIColor.init(red: 245/255, green: 245/255, blue: 245/255, alpha: 1.0)
        ImageView.layer.borderColor = UIColor.init(red: 220/255, green: 220/255, blue: 220/255, alpha: 1.0).cgColor
        ImageView.layer.borderWidth = 4.0
        continueShopBtn.layer.cornerRadius = 5.0
        let orderValue = orderDetails.object(forKey: "order") as! NSDictionary
        let str = (orderValue["productPrice"] as! String)
        let str1 =  str.replacingOccurrences(of: "$", with: "")
        orderLbl.text = "Order ID:" + String((orderValue["orderId"] as! Int))
        prodTtl.text = (orderValue["productName"] as! String)
        productImg.kf.setImage(with: URL(string: (orderValue["productImage"] as! String)))
        priceLbl.text =  "$ \(str1)"
        sizeLbl.text = (orderValue["productSize"] as! String)
        clrLbl.backgroundColor = color
        clrLbl.layer.borderWidth = 2.0
        clrLbl.layer.borderColor = (UIColor.init(red: 96/255, green: 96/255, blue: 96/255, alpha: 1.0)).cgColor
        QuantityLbl.text = String((orderValue["productQuantity"] as! String))
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let date = dateformatter.date(from: (orderValue.object(forKey: "orderDate") as! String))
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/YYYY"
        placedDteLbl.text = "Placed on:  \(formatter.string(from: date!))"
        
        let fullAddress = "\(shippingAddress["streetAddress"]!),  \(shippingAddress["extendedAddress"]!),\n\(shippingAddress["locality"]!),  \(shippingAddress["region"]!)\nZipcode:\(shippingAddress["postalCode"]!),  \(shippingAddress["countryCodeAlpha2"]!)"
        var attributedText = NSMutableAttributedString()
        attributedText = NSMutableAttributedString(string: "SHIPPING ADDRESS\n\(fullAddress)")
        attributedText.addAttribute(NSFontAttributeName, value:UIFont.init(name: "Helvetica Bold", size: 22)!, range: NSMakeRange(0, ("SHIPPING ADDRESS").characters.count))
        attributedText.addAttribute(NSForegroundColorAttributeName, value: (UIColor.init(red: 80/255, green: 80/255, blue: 80/255, alpha: 1.0)), range: NSMakeRange(0,("SHIPPING ADDRESS").characters.count))
        shippingLbl.attributedText = attributedText
        
        let fullAddress1 = "\(billingAddress["streetAddress"]!),  \(billingAddress["extendedAddress"]!),\n\(billingAddress["locality"]!),  \(billingAddress["region"]!)\nZipcode:\(billingAddress["postalCode"]!),  \(billingAddress["countryCodeAlpha2"]!)"
        var attributedText1 = NSMutableAttributedString()
        attributedText1 = NSMutableAttributedString(string: "BILLING ADDRESS\n\(fullAddress1)")
        attributedText1.addAttribute(NSFontAttributeName, value:UIFont.init(name: "Helvetica Bold", size: 22)!, range: NSMakeRange(0, ("BILLING ADDRESS").characters.count))
        attributedText1.addAttribute(NSForegroundColorAttributeName, value: (UIColor.init(red: 80/255, green: 80/255, blue: 80/255, alpha: 1.0)), range: NSMakeRange(0,("BILLING ADDRESS").characters.count))
        shipLbl.attributedText = attributedText1
    }
    

    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if context.nextFocusedView == continueShopBtn
        {
            continueShopBtn.layer.borderWidth = 5.0
            continueShopBtn.layer.borderColor = focusColor
        }
    }
    
    @IBAction func continueAction(_ sender: Any) {

        if fromPlayLayer
        {
            for viewcontroller in (self.navigationController?.viewControllers)!
            {
                if viewcontroller.isKind(of:PlayerLayerViewController.self)
                {
                    let  _ =  self.navigationController?.popToViewController(viewcontroller, animated: false)
                }
            }
        }
        else
        {
            for viewcontroller in (self.navigationController?.viewControllers)!
            {
                if viewcontroller.isKind(of:StoreViewController.self)
                {
                    let  _ =  self.navigationController?.popToViewController(viewcontroller, animated: false)
                }
            }
        }
       
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
