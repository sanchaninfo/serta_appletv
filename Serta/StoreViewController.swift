/***
 **Module Name: StoreViewController.
 **File Name :  StoreViewController.swift
 **Project :   Serta
 **Copyright(c) : Serta.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : Store page.
 */

import UIKit
import AVKit
import Kingfisher

class StoreViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    @IBOutlet var topView: UIView!
    @IBOutlet weak var transparentView: UIView!
    @IBOutlet weak var ProductPrice: UILabel!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var ProductDesc: UILabel!
    @IBOutlet weak var buyBtn: UIButton!
    @IBOutlet weak var ProductTiltle: UILabel!
    @IBOutlet weak var ProductLogo: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var ProductView: UIView!
    @IBOutlet weak var shopBtn: UIButton!
    @IBOutlet weak var closeBtn:UIButton!

    @IBOutlet weak var playeractiveBtn: UIButton!
    
    var userId = String()
    var playerLayer:AVPlayerLayer!
    var videoItemList = [AVPlayerItem]()
    var queuePlayer:AVQueuePlayer!
    var urlList = [String]()
    var playerItem:AVPlayerItem!
    var productData = NSDictionary()
    var data = NSArray()
    var productList = NSArray()
    var productCollectionList = NSMutableArray()
    var i:Int = 0
    var buyDict = NSDictionary()
    let focusGuide = UIFocusGuide()
    var seektime = Float64()
    var startAtInterval = Float64()
    var stopAtInterval = Float64()
    var isProductVisible = Bool()
    var isUseractive = Bool()
    let topButtonFocusGuide = UIFocusGuide()
    var isPlayerpaused = Bool()
    var isMenuPressed = Bool()
    var prodData = NSDictionary()
    var uuid,deviceID: String!
    var activityView = UIView()
    var collectionIndex = Int()
    var carousalLogo = String()
    
    var viewToFocus: UIView? = nil {
        didSet {
            if viewToFocus != nil {
                self.setNeedsFocusUpdate();
                self.updateFocusIfNeeded();
            }
        }
    }
    
//    override var preferredFocusEnvironments : [UIFocusEnvironment] {
//        //Condition
//        return [shopBtn]
//    }
    override func viewDidLoad() {
        super.viewDidLoad()
//        let logoImg = "http://s3.amazonaws.com/peafowl/aeom/cover.jpg"
//        logo.kf.setImage(with: URL(string: logoImg))
        activityView = ActivityView.init(frame: self.view.frame)
        self.view.addSubview(activityView)
        getProducts()
        setUpPlayerVideos()
        buyBtn.layer.cornerRadius = 7.0
        ProductView.layer.cornerRadius = 7.0
        shopBtn.layer.cornerRadius = min(shopBtn.frame.size.height, shopBtn.frame.size.width)/2.0
        shopBtn.clipsToBounds = true
        closeBtn.layer.cornerRadius = min(closeBtn.frame.size.height,closeBtn.frame.size.width)/2.0
        closeBtn.clipsToBounds = true
       
        if #available(tvOS 10.0, *) {
            topButtonFocusGuide.preferredFocusEnvironments = [buyBtn]
        } else {
            // Fallback on earlier versions
        }
        self.view.addLayoutGuide(topButtonFocusGuide)
        self.view.addConstraints([topButtonFocusGuide.topAnchor.constraint(equalTo: transparentView.topAnchor), topButtonFocusGuide.bottomAnchor.constraint(equalTo: transparentView.bottomAnchor), topButtonFocusGuide.leadingAnchor.constraint(equalTo: transparentView.leadingAnchor), topButtonFocusGuide.widthAnchor.constraint(equalTo: transparentView.widthAnchor)])
        collectionView.isHidden = true
        ProductView.isHidden = true
        shopBtn.isHidden = true
        closeBtn.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.activated), name: NSNotification.Name(rawValue: "ApplicationActivated"), object: nil)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleMenuPress))
        tapGesture.allowedPressTypes = [NSNumber(value: UIPressType.menu.rawValue)]
        self.view.addGestureRecognizer(tapGesture)
        
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(handlePausePress))
        tapGesture1.allowedPressTypes = [NSNumber(value: UIPressType.playPause.rawValue)]
        self.view.addGestureRecognizer(tapGesture1)
        
//         let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(handlePausePress))
//        tapGesture2.allowedPressTypes = [NSNumber(value:UIControl)]
        

    // Do any additional setup after loading the view.
    }
    func activated()
    {
        queuePlayer.play()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.queuePlayer.play()
    }
    
    func handleMenuPress()
    {
        queuePlayer.pause()
       // queuePlayer = nil
       let _ =  self.navigationController?.popViewController(animated: true)
    }
    func handlePausePress()
    {
        if (isPlayerpaused == false)
        {
            queuePlayer.pause()
            isPlayerpaused = true
        }
        else
        {
            queuePlayer.play()
            isPlayerpaused = false
        }
    }
    func setUpPlayerVideos()
    {
        videoItemList.removeAll()
        for dict in urlList
        {
            playerItem = AVPlayerItem.init(url: URL(string: dict)!)
            videoItemList.append(playerItem)
        }
        queuePlayer = AVQueuePlayer(items: videoItemList)
        playerLayer = AVPlayerLayer.init(player: queuePlayer)
        playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        playerLayer.zPosition = -1
        playerLayer.frame = CGRect(x: 0, y: 0, width: 1920, height: 1080)
        topView.layer.addSublayer(playerLayer)
        queuePlayer.isMuted = false
        queuePlayer.play()
        NotificationCenter.default.addObserver(self, selector: #selector(self.loopVideo(sender:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: queuePlayer.items().last)
    
        
                queuePlayer?.addPeriodicTimeObserver(forInterval: CMTimeMake(1,1), queue: DispatchQueue.main, using:
                    {_ in
                        if self.queuePlayer.currentItem?.status == .readyToPlay
                        {
                            self.activityView.removeFromSuperview()
                            self.seektime = CMTimeGetSeconds((self.queuePlayer.currentItem?.currentTime())!)
                            let currentT = Int(self.seektime)
                            if (currentT >= Int(self.startAtInterval)) && (self.isProductVisible == false) && (currentT <= Int(self.stopAtInterval)) //&& (self.isUseractive == false)
                            {
                               self.shopBtn.isHidden = false
                               self.closeBtn.isHidden = false
                               self.playeractiveBtn.isHidden = true
                               self.viewToFocus = self.shopBtn
                               self.isProductVisible = true
                                self.isMenuPressed = true
                            //   self.isUseractive = true
                            }
                            
                            else if (currentT >= Int(self.stopAtInterval)) && (self.isProductVisible == true)// && (self.isUseractive == true)
                            {
                                self.shopBtn.isHidden = true
                                self.closeBtn.isHidden = true
//                                if (self.collectionView.isDecelerating)
//                                {
//                                    self.collectionView.isHidden = false
//                                    self.ProductView.isHidden = false
//                                }
//                                else
//                                {
                                    self.collectionView.isHidden = true
                                    self.ProductView.isHidden = true
                                    self.playeractiveBtn.isHidden = false
                                    self.viewToFocus = self.playeractiveBtn
                                 //   self.isUseractive = false
                                    self.changeProducts()
                               // }
                            }
                        }
                })
    }
 
    func loopVideo(sender:NSNotification) {
        
        queuePlayer.removeAllItems()
        queuePlayer = nil
        playerLayer.removeFromSuperlayer()
        playerItem = nil
        setUpPlayerVideos()
    }
    func getActive()
    {
        if shopBtn.isHidden
        {
            shopBtn.isHidden = false
        }
        else
        {
            shopBtn.isHidden = true
        }
   
    }
//    func appenterforeground()
//    {
//        viewWillAppear(true)
//    }
    func getProducts()
    {
//        data = productData.object(forKey: "dataa") as! NSArray
//        
//        for carousal in data
//        {
//            urlList.append((carousal as! NSDictionary)["videoURL"] as! String)
//        }
        
        urlList.append(prodData["videoURL"] as! String)
       // let carousal = data[0] as! NSDictionary
        let individualcarousel = prodData.object(forKey: "carousels") as! NSArray
        let carousalid = individualcarousel[0] as! NSDictionary
        let startAt = carousalid["startAt"] as! String
        let stopAt = carousalid["stopAt"] as! String
        
        
        let startT = startAt.components(separatedBy: ":")
        let starthour = (Int(startT[0]))! * 60 * 60
        let startminute = Int(startT[1])! * 60
        let startseconds = Int(startT[2])!
        startAtInterval = Float64(starthour + startminute + startseconds)
        
        
        let stopT = stopAt.components(separatedBy: ":")
        let stophour = (Int(stopT[0]))! * 60 * 60
        let stopminutes = Int(stopT[1])! * 60
        let stopseconds = Int(stopT[2])!
        stopAtInterval = Float64(stophour + stopminutes + stopseconds)
        
        productList = carousalid["products"] as! NSArray
        for individualProduct in productList
        {
            let dict = individualProduct as! NSDictionary
            productCollectionList.add(dict)
        }
        
        self.shopBtn.setTitle("SHOP THE COLLECTION(\(productCollectionList.count))", for: .normal)
    }
    
    func changeProducts()
    {
        self.isProductVisible = false
        productCollectionList.removeAllObjects()
//        let data = productData.object(forKey: "dataa") as! NSArray
//        let carousal = data[0] as! NSDictionary
        let individualcarousel = prodData.object(forKey: "carousels") as! NSArray
        if i <=  (individualcarousel.count)
        {
            i = i + 1
            if (i == (individualcarousel.count))
            {
               i = 0
            }
        }
        let carousalid = individualcarousel[i] as! NSDictionary
        let startAt = carousalid["startAt"] as! String
        let stopAt = carousalid["stopAt"] as! String
        
    
        let aaa = startAt.components(separatedBy: ":")
        let starthour = (Int(aaa[0]))! * 60 * 60
        let startminute = Int(aaa[1])! * 60
        let startseconds = Int(aaa[2])!
        startAtInterval = Float64(starthour + startminute + startseconds)
        let bbb = stopAt.components(separatedBy: ":")
        let stophour = (Int(bbb[0]))! * 60 * 60
        let stopminutes = Int(bbb[1])! * 60
        let stopseconds = Int(bbb[2])!
        stopAtInterval = Float64(stophour + stopminutes + stopseconds)
            productList = carousalid["products"] as! NSArray
            for individualProduct in productList
            {
                let dict = individualProduct as! NSDictionary
                productCollectionList.add(dict)
            }
        self.shopBtn.setTitle("SHOP THE COLLECTION(\(productCollectionList.count))", for: .normal)
        collectionView.reloadData()
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productCollectionList.count

    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        let path = productCollectionList[indexPath.row] as! NSDictionary
        let carousalImage = path["images"] as! NSArray
        let imageThumb = carousalImage[0] as! NSDictionary
        let imageUrl = kStoreBaseUrl + (imageThumb["thumb"] as! String)
        let img = (cell.viewWithTag(10) as! UIImageView)
        img.kf.indicatorType = .activity
        (cell.viewWithTag(10) as! UIImageView).kf.setImage(with: URL(string: imageUrl))
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didUpdateFocusIn context: UICollectionViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if let prev = context.previouslyFocusedIndexPath,
            let cell = collectionView.cellForItem(at: prev)
        {
           
            (cell.viewWithTag(10) as! UIImageView).transform = .identity
            
        }
        if let next = context.nextFocusedIndexPath,
            let cell = collectionView.cellForItem(at: next)
        {

            let individualcarousel = prodData.object(forKey: "carousels") as! NSArray
            let carousalDict = individualcarousel[i] as! NSDictionary
            self.carousalLogo = carousalDict["carouselLogo"] as! String
            self.ProductLogo.kf.setImage(with: URL(string: carousalLogo))
            let path = productCollectionList[next.row] as! NSDictionary
            buyDict = path
            ProductTiltle.text = path["title"] as? String
            ProductPrice.text = "$" + "\(path["price"] as! String)"
            (cell.viewWithTag(10) as! UIImageView).adjustsImageWhenAncestorFocused = true
            collectionIndex = next.row
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let Path = productCollectionList[indexPath.row] as! NSDictionary
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let productdetailpage = storyBoard.instantiateViewController(withIdentifier: "productdetails") as! StoreProductDetailsViewController
        productdetailpage.productlist = Path
        productdetailpage.userId = userId
        productdetailpage.uuid = uuid
        productdetailpage.deviceId = deviceID
        productdetailpage.imgLogo = self.carousalLogo
        self.queuePlayer.pause()
        self.navigationController?.pushViewController(productdetailpage, animated: true)
    }
    @IBAction func buyAction(_ sender: AnyObject) {
        let Path = buyDict
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let detailpage = storyBoard.instantiateViewController(withIdentifier: "StoreDetail") as! StoreDetailViewController
        detailpage.productlist = Path
        detailpage.userId = userId
        detailpage.uuid = uuid
        detailpage.deviceId = deviceID
        queuePlayer.pause()
        self.navigationController?.pushViewController(detailpage, animated: true)
    }
    @IBAction func shopAction(_ sender: AnyObject) {
        shopBtn.isHidden = true
        closeBtn.isHidden = true
        collectionView.isHidden = false
        ProductView.isHidden = false
        viewToFocus = collectionView
    }
    
    
    @IBAction func closeBtn(_ sender: Any) {
        shopBtn.isHidden = true
        closeBtn.isHidden = true
    }
    
    @IBAction func playerBtn(_ sender: Any) {
       if (isPlayerpaused == false)
       {
        queuePlayer.pause()
        isPlayerpaused = true
       }
        else
       {
        queuePlayer.play()
        isPlayerpaused = false
       }
    }
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if context.nextFocusedView == buyBtn
        {
//            buyBtn.layer.borderColor = focusColor
//            buyBtn.layer.borderWidth = 5.0
            buyBtn.bounds = CGRect(x: buyBtn.bounds.origin.x, y: buyBtn.bounds.origin.y, width: buyBtn.bounds.size.width + 15, height: buyBtn.bounds.size.height + 15)
        }
        if context.nextFocusedView == shopBtn
        {
          //  shopBtn.backgroundColor = UIColor.init(red: 231/255, green: 0/255, blue: 37/255, alpha: 1)
         //   shopBtn.bounds = CGRect(x: shopBtn.bounds.origin.x, y: shopBtn.bounds.origin.y, width: shopBtn.bounds.size.width + 5, height: shopBtn.bounds.size.height + 5)
            shopBtn.layer.borderWidth = 4.0
            shopBtn.layer.borderColor = UIColor.white.cgColor
            
        }
        if context.nextFocusedView == closeBtn
        {
          //  closeBtn.bounds = CGRect(x: closeBtn.bounds.origin.x, y: closeBtn.bounds.origin.y, width: closeBtn.bounds.size.width + 5, height: closeBtn.bounds.size.height + 5)
            closeBtn.layer.borderWidth = 4.0
            closeBtn.layer.borderColor = UIColor.white.cgColor
        }
        if context.previouslyFocusedView == buyBtn
        {
          //   buyBtn.layer.borderWidth = 0.0
            buyBtn.bounds = CGRect(x: buyBtn.bounds.origin.x, y: buyBtn.bounds.origin.y, width: buyBtn.bounds.size.width - 15, height: buyBtn.bounds.size.height - 15)
        }
        if context.previouslyFocusedView == shopBtn
        {
           //  shopBtn.bounds = CGRect(x: shopBtn.bounds.origin.x, y: shopBtn.bounds.origin.y, width: shopBtn.bounds.size.width - 5, height: shopBtn.bounds.size.height - 5)
             shopBtn.layer.borderWidth = 0.0
             shopBtn.layer.borderColor = UIColor.clear.cgColor
        }
        if context.previouslyFocusedView == closeBtn
        {
           //  closeBtn.bounds = CGRect(x: closeBtn.bounds.origin.x, y: closeBtn.bounds.origin.y, width: closeBtn.bounds.size.width - 5, height: closeBtn.bounds.size.height - 5)
            closeBtn.layer.borderWidth = 0.0
            closeBtn.layer.borderColor = UIColor.clear.cgColor
        }
//        if context.nextFocusedView == collectionView
//        {
//            viewToFocus = collectionView.cellForItem(at: IndexPath(row: collectionIndex, section: 0))
//        }
    }
    deinit {
        
         }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
