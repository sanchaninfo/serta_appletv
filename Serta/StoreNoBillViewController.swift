/***
 **Module Name: StoreNoBillViewController.
 **File Name :  StoreNoBillViewController.swift
 **Project :   Serta
 **Copyright(c) : Serta.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : Store No Bill Page.
 */

import UIKit

class StoreNoBillViewController: UIViewController {

    @IBOutlet var gotoLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        gotoLbl.text = "Go to \(kBaseUrl)"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func okbtn(_ sender: Any) {
     let _ = self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
