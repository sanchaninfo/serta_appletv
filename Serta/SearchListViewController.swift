/***
 **Module Name:  SearchList ViewController.
 **File Name :  SearchListViewController.swift
 **Project :   Serta
 **Copyright(c) : Serta.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : Search Page.
 */

import UIKit

class SearchListViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UISearchResultsUpdating,UISearchBarDelegate {
    @IBOutlet weak var collectionView: UICollectionView!
    
    var searchController : UISearchController!
    var filteredDataItems = Array<Any>()
    var searchCollectionList = NSMutableArray()
    var userId = String()
    var deviceId,uuid: String!
    var searchDelegate: myListdelegate!
    var storeProdData = [[String:Any]]()
    var Donatedict = [[String:Any]]()
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        searchBar.barTintColor = UIColor.white
        let lists:Array = searchCollectionList as Array
        let searchPredicate = NSPredicate(format: "name CONTAINS[C] %@", searchText)
        let array = (lists as NSArray).filtered(using: searchPredicate)
        filteredDataItems = array
        collectionView.reloadData()
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        filteredDataItems = searchCollectionList as Array
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return filteredDataItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        let imagePath = filteredDataItems[indexPath.row] as! NSDictionary
        let image = imagePath["metadata"] as! NSDictionary
        (cell.viewWithTag(20) as! UILabel).text = (image["title"] as! String)
        if (image[kMovieart] as! String == "")
        {
            // (cell.viewWithTag(11) as! UIImageView).kf.setImage(with: URL(string: metaDataPath[kMovieart] as! String))
            (cell.viewWithTag(21) as! UIImageView).image = UIImage(imageLiteralResourceName:"RevoltFlat")
        }
        else
        {
            (cell.viewWithTag(21) as! UIImageView).kf.setImage(with: URL(string: image[kMovieart] as! String))
        }
//        (cell.viewWithTag(21) as! UIImageView).kf.setImage(with: URL(string: image["movie_art"] as! String))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didUpdateFocusIn context: UICollectionViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator)
    {
        if let prev = context.previouslyFocusedIndexPath,
            let cell = collectionView.cellForItem(at: prev)
        {
            cell.transform = .identity
        }
        if let next = context.nextFocusedIndexPath,
            let cell = collectionView.cellForItem(at: next)
        {
            (cell.viewWithTag(21) as! UIImageView).adjustsImageWhenAncestorFocused = true
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let Path = filteredDataItems[indexPath.row] as! NSDictionary
        getaccountInfo(id:Path["id"] as! String,userid: userId/*,tvshow: (Path["tv_show"] as! Bool)*/)
        
        // getAssetData(withUrl:kAssestDataUrl,id: Path["id"] as! String,userid: userId,tvshow:(Path["tv_show"] as! Bool))
    }
    
    func updateSearchResults(for searchController: UISearchController)
    {
        searchController.searchBar.delegate = self
    }
    
    func getaccountInfo(id:String,userid:String/*tvshow:Bool*/)
    {
        var parameters =  [String:[String:AnyObject]]()
        parameters = ["getAccountInfo":["deviceId":deviceId as AnyObject,"uuid":uuid as AnyObject]]
        ApiManager.sharedManager.postDataWithJson(url: kAccountInfoUrl, parameters: parameters){
            (responseDict,error,isDone) in
            if error == nil
            {
                let Json = responseDict
                let dict = Json as! NSDictionary
                accountresponse = (dict.value(forKey: "uuid_exist") as! Bool)
                if accountresponse == true
                {
                    self.getAssetData(withUrl:kAssestDataUrl,id: id,userid: userid/*,tvshow:tvshow*/)
                }
                else
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.gotoCode()
                }
            }
        }
        
    }

    func getAssetData(withUrl:String,id:String,userid:String/*,tvshow:Bool*/)
    {
        var parameters =  [String:[String:AnyObject]]()
        parameters = ["getAssestData":["videoId":id as AnyObject,"userId":userid as AnyObject]]
        ApiManager.sharedManager.postDataWithJson(url: withUrl, parameters: parameters){(responseDict , error,isDone) in
            if error == nil
            {
                let JSON = responseDict
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let DetailPage = storyBoard.instantiateViewController(withIdentifier: "DetailPage") as! DetailPageViewController
                
                if JSON is NSArray
                {
                    let dict = JSON as! NSArray
                    DetailPage.TvshowPath = dict.firstObject as! NSDictionary
                }
                else
                {
                    DetailPage.TvshowPath = JSON as! NSDictionary
                }
                DetailPage.userid = self.userId
                DetailPage.deviceId = self.deviceId
                DetailPage.uuid = self.uuid
                DetailPage.delegate = self.searchDelegate
                DetailPage.storeProdData = self.storeProdData
                DetailPage.Donatedict = self.Donatedict
                DetailPage.fromSearch = true
              //  self.view.window = UIWindow(frame: UIScreen.main.bounds)
                
               // let nav1 = UINavigationController()
               // let mainView = ViewController(nibName: "Main", bundle: nil) //ViewController = Name of your controller
                //nav1.viewControllers = [mainView]
                //self.view.window!.rootViewController = nav1
              //  self.view.window?.makeKeyAndVisible()
              //  nav1.pushViewController(DetailPage, animated: true)
              //  self.present(DetailPage, animated: true, completion: nil)
               // self.navigationController?.pushViewController(DetailPage, animated: true)
              // self.navigationController?.pushViewController(DetailPage, animated: true)
               // self.navigationController?.present(DetailPage, animated: true, completion: nil)
              //  self.navigationController?.pushViewController(DetailPage, animated: true)
                self.present(DetailPage, animated: true, completion: nil)
                
            }
            else
            {
//                print("json Error")
//                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
//                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
//                    UIAlertAction in
//                let _ = self.navigationController?.popViewController(animated: true)
//                })
//                alertview.addAction(defaultAction)
//                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
//extension SearchListViewController
//{
//    func myListdata(mylistDict: NSDictionary) {
//        
//    }
//    func removeListdata(id : String) {
//        
//    }
//    func recentlyWatcheddata(recentlyWatchedDict: NSDictionary) {
//        
//    }
//}
