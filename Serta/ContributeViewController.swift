/***
 **Module Name: ContributionViewController.
 **File Name :  ContributionViewController.swift
 **Project :   Serta
 **Copyright(c) : Serta.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : Contribution Details..
 */

import UIKit

class ContributeViewController: UIViewController {
    @IBOutlet var ContributeValue: UITextField!
    @IBOutlet var confirmBtn: UIButton!
    var donationDict = NSDictionary()
    var accountDict = NSDictionary()
    @IBOutlet var Img: UIImageView!
    @IBOutlet var valueLbl: UILabel!
    override func viewDidLoad() {
        
        confirmBtn.layer.cornerRadius = 3.0
      
        if donationDict.count != 0
        {
            fixedDonation()
        }
        else
        {
            variableDonation()
        }
       
        super.viewDidLoad()
        let text = ContributeValue.text
        if !((text?.isEmpty)!)
        {
           
        }
        // Do any additional setup after loading the view.
    }

    func fixedDonation()
    {
        ContributeValue.isHidden = true
        let imgarray = donationDict.object(forKey: "images") as! NSArray
        let imgDict = imgarray.firstObject as! NSDictionary
        let imgstr = kStoreBaseUrl + (imgDict.value(forKey: "thumb") as! String)
        Img.kf.setImage(with: URL(string: imgstr))
        valueLbl.text = "Click Confirm to donate an amount of $ \(donationDict.value(forKey: "price") as! String)"
    }
    
    func variableDonation()
    {
        let value = ContributeValue.text
        valueLbl.text = "Click Confirm to donate an amount of $\(value!)"
        Img.kf.setImage(with: URL(string: (kStoreBaseUrl) + "/cover.jpg"))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func confirmdonate(_ sender: Any) {
        let contibute = Int(ContributeValue.text!)
        var parameters = [String:[String:Any]]()
        if donationDict.count != 0
        {
            let imgarray = donationDict.object(forKey: "images") as! NSArray
            let imgDict = imgarray.firstObject as! NSDictionary
            let imgstr = kStoreBaseUrl + (imgDict.value(forKey: "thumb") as! String)
            parameters =
                ["payWithCVVDonation":["amount":(donationDict.value(forKey: "price") as! String),"transaction_id":"","date":"","email":(accountDict.value(forKey: "email") as! String),"billingAddress":"","shippingAddress":"","userName":(accountDict.value(forKey: "full_name") as! String),"productName":"","productPrice":"","productColor":"","productSize":"","productQuantity":"","productDeliveryCharge":"","userPhoneNumber":"","productImage":imgstr,"cvv":"123","userId":(accountDict.value(forKey: "_id") as! String),"customerId":(accountDict.value(forKey: "customer_id") as! String)]]
            
        }
        else
        {
            if contibute != 0
            {
                parameters = ["payWithCVVDonation":["amount":ContributeValue.text!,"transaction_id":"","date":"","email":(accountDict.value(forKey: "email") as! String),"billingAddress":"","shippingAddress":"","userName":(accountDict.value(forKey: "full_name") as! String),"productName":"","productPrice":"","productColor":"","productSize":"","productQuantity":"","productDeliveryCharge":"","userPhoneNumber":"","productImage":"","cvv":"123","userId":(accountDict.value(forKey: "_id") as! String),"customerId":(accountDict.value(forKey: "customer_id") as! String)]]
            }
            
        }
        ApiManager.sharedManager.postDataWithJson(url: kPayDonate, parameters: parameters as [String : [String : AnyObject]]){(responseDict, error, isDone) in
            if error == nil
            {
                let json = responseDict as! NSDictionary
           
                if ((json.value(forKey: "success") as! Bool))
                {
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let DS = storyBoard.instantiateViewController(withIdentifier: "DonationSummary") as! DonationSummaryViewController
                    DS.summaryDict = json
                    self.navigationController?.pushViewController(DS, animated: true)
                }
            }
            else
            {
                print("JSON ERROR")
            }
        
    }
    }
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if context.nextFocusedView == confirmBtn
        {
            confirmBtn.layer.borderWidth = 5.0
            confirmBtn.layer.borderColor = focusColor
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


}
