/***
 **Module Name: CustomAlertView
 **File Name :   CustomAlertView.swift
 **Project :   Serta
 **Copyright(c) : Serta.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : custom alert view.
 */

import Foundation
import UIKit

class CustomAlertView: UIView
{
    var label = UILabel()
    override  init(frame: CGRect) {
        super.init(frame:frame)
        self.frame = frame
        self.backgroundColor = UIColor.black
        self.alpha = 0.6
    
        let alertView = UIView(frame: CGRect(x: 560, y: 290, width: 800, height: 500))
        alertView.backgroundColor = UIColor.gray
        alertView.layer.cornerRadius = 7.0
        
        label = UILabel(frame: CGRect(x: 0, y: 0, width: alertView.frame.size.width, height: 250))
        label.numberOfLines = 3
        label.textAlignment = .center
        label.textColor = UIColor.white
       
        label.font = UIFont(name: "Helvetica Bold", size: 30)
        alertView.addSubview(label)
        
        let button = UIButton(type: .system)
        button.frame = CGRect(x: 150, y: 300, width: 500, height: 60)
        button.setTitle("OK", for: .normal)
        button.setTitleColor(UIColor.black, for: .normal)
        button.backgroundColor = UIColor.white
        button.addTarget(self, action: #selector(clicked(sender:)), for: .primaryActionTriggered)
        alertView.addSubview(button)
        self.addSubview(alertView)
    }
    
    convenience init ()
    {
        self.init(frame:CGRect.zero)
    }
    
    func setTitle(message:String)
    {
        label.text = message
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    func clicked(sender:AnyObject)
    {
        exit(0)
    }

}
