/***
 **Module Name:  SettingsViewController.
 **File Name : SettingsViewController.swift
 **Project :   Serta
 **Copyright(c) : Serta.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : setting Page.
 */

import UIKit

class SettingsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var menu = [String]()
    var deviceId,uuid : String!
    override func viewDidLoad() {
        super.viewDidLoad()
        menu.append("My Account")
        menu.append("Get help")
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        (cell.viewWithTag(11) as! UILabel).text = menu[indexPath.row]
        cell.layer.cornerRadius = 7.0
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didUpdateFocusIn context: UITableViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if let previous = context.previouslyFocusedIndexPath,
            let cell = tableView.cellForRow(at: previous)
        {
            (cell.viewWithTag(11) as! UILabel).textColor = UIColor.white
        }
        if let next = context.nextFocusedIndexPath,
            let cell = tableView.cellForRow(at: next)
        {
            (cell.viewWithTag(11) as! UILabel).textColor = UIColor.black
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0
        {
            accountstatus()
        }
    }
    
    func accountstatus()
    {
        let url = kAccountInfoUrl
        let  parameters = [ "getAccountInfo": ["deviceId": deviceId!, "uuid": uuid!]]
        ApiManager.sharedManager.postDataWithJson(url: url, parameters: parameters as [String : [String : AnyObject]]) {(responseDict , error,isDone) in
            if error == nil
            {
                let post = responseDict
                let dict = post as! NSDictionary
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let accountInfo = storyBoard.instantiateViewController(withIdentifier: "Account") as! AccountInfoViewController
                accountInfo.accountDict = dict
                if (dict["uuid_exist"] as! Bool) == true
                {
                    self.navigationController?.pushViewController(accountInfo, animated: true)
                }
                else
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.gotoCode()
                }
        }
            else
            {
                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                   let _ = self.navigationController?.popViewController(animated: true)
                })
                alertview.addAction(defaultAction)
                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
